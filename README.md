GLI TP3 GWT 
===================
TP de mise en place d'une interface pour une application web avec **GWT**.

----------
[TOC]

----------

##1. Présentation

Ce projet est un site web qui permet de faire des appels à l'application web dévelloper dans le cours de TAA 

----------

##2. Fonctionnalités 

Le site offre plusieurs fonctionnalités qui sont les suivantes :
    - Lister les entitées *Language*, *User* et *Sprint*
    - Supprimer les entitées *Language*, *User* et *Sprint*
    - Ajouter l'entitées *Language*

----------

##3. Utiliser le site web

###3.1. Général 
Pour utiliser l'application il faut lancer la commande `mvn gwt:run` cela va ouvrir la la console de dévelopement.