package fr.istic.gla.gli.client.utils.clickHandlerImpl;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import fr.istic.gla.gli.client.tp3_gwtUi;

/**
 * Created by lautre on 18/01/16.
 */
public class refreshListClickHandler implements ClickHandler {

    private final tp3_gwtUi parent;

    public refreshListClickHandler(tp3_gwtUi parent) {
        this.parent = parent;
    }

    public void onClick(ClickEvent clickEvent) {
        parent.listEntity();
    }
}
