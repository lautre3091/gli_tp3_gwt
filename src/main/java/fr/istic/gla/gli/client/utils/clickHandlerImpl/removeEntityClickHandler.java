package fr.istic.gla.gli.client.utils.clickHandlerImpl;

import com.github.gwtbootstrap.client.ui.Alert;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import fr.istic.gla.gli.client.service.tp3_gwtServiceAsync;
import fr.istic.gla.gli.client.tp3_gwtUi;

/**
 * Created by lautre on 17/01/16.
 */
public class removeEntityClickHandler implements ClickHandler {

    private final tp3_gwtUi parent;
    private tp3_gwtServiceAsync serviceAsync;
    private String entity;
    private String id;

    private AsyncCallback<Void> removeEntityAsyncCallback = new AsyncCallback<Void>() {
        public void onFailure(Throwable throwable) {
            parent.showError();
        }

        public void onSuccess(Void aVoid) {
            Alert alert = parent.getAlertList();
            alert.setHeading("Info");
            alert.setType(AlertType.INFO);
            alert.setText("Entity removed !");
            alert.setVisible(true);
            alert.setClose(false);

            try {
                parent.getAllEntity(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public removeEntityClickHandler(tp3_gwtServiceAsync service, String entity, String id, tp3_gwtUi parent) {
        this.parent = parent;
        this.serviceAsync = service;
        this.entity = entity;
        this.id = id;
    }

    public void onClick(ClickEvent clickEvent) {
        serviceAsync.deleteOneById(entity,id,removeEntityAsyncCallback);
    }
}
