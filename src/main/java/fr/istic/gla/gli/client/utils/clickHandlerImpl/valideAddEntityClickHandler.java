package fr.istic.gla.gli.client.utils.clickHandlerImpl;

import com.github.gwtbootstrap.client.ui.Alert;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import fr.istic.gla.gli.client.service.tp3_gwtServiceAsync;
import fr.istic.gla.gli.client.tp3_gwtUi;

import java.util.HashMap;

/**
 * Created by lautre on 18/01/16.
 */
public class valideAddEntityClickHandler implements ClickHandler{

    private final tp3_gwtUi parent;
    private tp3_gwtServiceAsync serviceAsync;
    private String entity;

    private AsyncCallback<Void> removeEntityAsyncCallback = new AsyncCallback<Void>() {
        public void onFailure(Throwable throwable) {
            parent.showError();
        }

        public void onSuccess(Void aVoid) {
            Alert alert = parent.getAlertList();
            alert.setHeading("Info");
            alert.setType(AlertType.INFO);
            alert.setText("Entity Add !");
            alert.setVisible(true);
            alert.setClose(false);

            parent.listEntity();
            parent.hideAllForm();
        }
    };

    public valideAddEntityClickHandler(tp3_gwtServiceAsync service, String entity, tp3_gwtUi parent) {
        this.parent = parent;
        this.serviceAsync = service;
        this.entity = entity;
    }

    public void onClick(ClickEvent clickEvent) {
        HashMap<String, String> data = null;
        if (entity.equals("language")) data = addLanguage();

        serviceAsync.create(entity,data,removeEntityAsyncCallback);
    }

    private HashMap<String, String> addLanguage(){
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("name",parent.nameLanguage());
        return data;
    }
}
