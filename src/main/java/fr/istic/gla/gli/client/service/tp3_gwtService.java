package fr.istic.gla.gli.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.HashMap;

@RemoteServiceRelativePath("tp3_gwtService")
public interface tp3_gwtService extends RemoteService {
    // Sample interface method of remote interface

    String getAll(String entity) throws Exception;
    String getOneById(String entity,String id) throws Exception;

    void create(String entity, HashMap<String,String> data) throws Exception;

    void deleteOneById(String entity,String id)throws Exception;
}
