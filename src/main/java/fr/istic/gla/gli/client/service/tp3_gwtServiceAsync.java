package fr.istic.gla.gli.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.HashMap;

public interface tp3_gwtServiceAsync {

    void getAll(String entity, AsyncCallback<String> async) throws Exception;
    void getOneById(String entity,String id, AsyncCallback<String> async) throws Exception;

    void create(String entity, HashMap<String, String> data, AsyncCallback<Void> async);

    void deleteOneById(String entity, String id, AsyncCallback<Void> async);
}
