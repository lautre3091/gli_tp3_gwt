package fr.istic.gla.gli.client;

import com.github.gwtbootstrap.client.ui.*;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.github.gwtbootstrap.client.ui.constants.ButtonType;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.HeadingElement;
import com.google.gwt.dom.client.InputElement;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.ListBox;
import fr.istic.gla.gli.client.service.tp3_gwtService;
import fr.istic.gla.gli.client.service.tp3_gwtServiceAsync;
import fr.istic.gla.gli.client.utils.clickHandlerImpl.*;

/**
 * Created by lautre on 14/12/15.
 */
public class tp3_gwtUi extends Composite {


    public ListBox getSelectEntity() {
        return selectEntity;
    }

    private enum Entity {
        LANGUAGE("Language","language"),
        USER("User","user"),
        USER_STORIE("User Storie","userStorie"),
        TASK("Task","task"),
        SPRINT("Sprint","sprint");

        private String name;
        private String id;

        Entity(String name,String id){
            this.name = name;
            this.id = id;
        }
    }

    private final tp3_gwtServiceAsync service;

    @UiField ListBox selectEntity;
    @UiField HeadingElement selectedEntity;
    @UiField Container listEntity;
    @UiField Alert alertList;
    @UiField Button buttonAdd;
    @UiField Button buttonRefresh;
    @UiField
    Row formAddLanguage;
    @UiField
    InputElement nameLanguage;
    @UiField
    Button validAddLanguage;
    @UiField
    Button cancelAddLanguage;


    HandlerRegistration handlerRefreshButton;
    HandlerRegistration handlerAddButton;

    public Alert getAlertList(){
        return alertList;
    }

    public String nameLanguage(){
        return nameLanguage.getValue();
    }

    interface tp3_gwtUiUiBinder extends UiBinder<HTMLPanel, tp3_gwtUi> {}

    private static tp3_gwtUiUiBinder ourUiBinder = GWT.create(tp3_gwtUiUiBinder.class);

    public tp3_gwtUi() {

        service = GWT.create(tp3_gwtService.class);
        ServiceDefTarget endpoint = (ServiceDefTarget) service;
        endpoint.setServiceEntryPoint(GWT.getModuleBaseURL() + "getData");

        initWidget(ourUiBinder.createAndBindUi(this));

        for (Entity entity : Entity.values()) selectEntity.addItem(entity.name,entity.id);

        handlerRefreshButton = buttonRefresh.addClickHandler(new refreshListClickHandler(this));
        handlerAddButton = buttonAdd.addClickHandler(new addEntityClickHandler(this));

        validAddLanguage.addClickHandler(new valideAddEntityClickHandler(service,"language",this));
        cancelAddLanguage.addClickHandler(new cancelShowFormClickHandler(this,"language"));
        formAddLanguage.getElement().getStyle().setDisplay(Style.Display.NONE);

    }

    @UiHandler("selectEntity")
    public void onChange(ChangeEvent event) {
        listEntity();
    }

    public void getAllEntity (final String id) throws Exception {
        listEntity.clear();
        service.getAll(id,new AsyncCallback<String>() {

            public void onFailure(Throwable e) {
                showError();
            }
            public void onSuccess(String data) {
                JSONArray jsonArray = JSONParser.parseStrict(data).isArray();
                if (jsonArray != null && jsonArray.size() > 0 ){
                    if (id.equals("language")) listLanguage(jsonArray);
                    else if (id.equals("sprint")) listSprint(jsonArray);
                    else if (id.equals("task")) listTask(jsonArray);
                    else if (id.equals("user")) listUser(jsonArray);
                    else if (id.equals("userStorie")) listUserStorie(jsonArray);

                } else {
                    showWarning("Nothing to show !");
                }
            }
        });
    }

    public void showError(){
        alertList.setHeading("Error");
        alertList.setType(AlertType.ERROR);
        alertList.setText("Server call failed !");
        alertList.setVisible(true);
        alertList.setClose(false);
    }

    public void showWarning(String msg){
        alertList.setHeading("Warning");
        alertList.setType(AlertType.WARNING);
        alertList.setText(msg);
        alertList.setVisible(true);
        alertList.setClose(false);
    }

    private void listLanguage(JSONArray data) {
        for (int i=0;i<data.size();i++){
            JSONObject jsonObject = data.get(i).isObject();
            Row row = new Row();
            Column c1 = new Column(2);
            c1.add(new Paragraph("Name : "+getString(jsonObject,"name")));
            Column c2 = new Column(5);
            c2.add(new Paragraph("Used by "+ String.valueOf(jsonObject.get("users").isArray().size() + " user" )));
            row.add(c1);
            row.add(c2);
            row.add(createButtonRemove("language",jsonObject.get("id").toString()));
            row.setMarginTop(5);
            listEntity.add(row);
        }
        

    }

    private void listUserStorie(JSONArray data) {

    }

    private void listUser(JSONArray data) {

        for (int i=0;i<data.size();i++){
            JSONObject jsonObject = data.get(i).isObject();
            Row row = new Row();
            Column c1 = new Column(2);
            c1.add(new Paragraph("Name : "+getString(jsonObject,"prenom")+" "+getString(jsonObject,"nom")));
            Column c2 = new Column(5);
            c2.add(new Paragraph("Contains "+ String.valueOf(jsonObject.get("userStories").isArray().size() + " User Stories" )));
            row.add(c1);
            row.add(c2);
            row.add(createButtonRemove("user",jsonObject.get("id").toString()));
            row.setMarginTop(5);
            listEntity.add(row);
        }
    }

    private void listTask(JSONArray data) {

    }

    private void listSprint(JSONArray data) {
        for (int i=0;i<data.size();i++){
            JSONObject jsonObject = data.get(i).isObject();
            Row row = new Row();
            Column c1 = new Column(2);
            c1.add(new Paragraph("Name : "+getString(jsonObject,"nom")));
            Column c2 = new Column(5);
            c2.add(new Paragraph("Have "+ String.valueOf(jsonObject.get("userStories").isArray().size() + " user storie" )));
            row.add(c1);
            row.add(c2);
            row.add(createButtonRemove("user",jsonObject.get("id").toString()));
            row.setMarginTop(5);
            listEntity.add(row);
        }
    }

    public void listEntity(){
        hideAllForm();
        selectedEntity.setInnerHTML(selectEntity.getSelectedItemText());
        handlerRefreshButton.removeHandler();
        handlerRefreshButton = buttonRefresh.addClickHandler(new refreshListClickHandler(this));
        try {
            alertList.setVisible(false);
            getAllEntity(selectEntity.getSelectedValue());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showFromAddEntity(){
        hideAllForm();
        String id = getSelectEntity().getSelectedValue();
        if (id.equals("language")) getFormAddLanguage().getElement().getStyle().setDisplay(Style.Display.BLOCK);

    }

    public void hideAllForm(){

        getFormAddLanguage().getElement().getStyle().setDisplay(Style.Display.NONE);
    }

    private Button createButtonRemove (String entity,String id){
        Button button = new Button();
        button.setType(ButtonType.DANGER);
        button.setText("Remove");
        button.setIcon(IconType.REMOVE_CIRCLE);
        button.addClickHandler(new removeEntityClickHandler(service,entity,id,this));
        return button;
    }

    private String getString(JSONObject jsonObject,String key){
        return jsonObject.get(key).toString().replace("\"","");
    }

    public Row getFormAddLanguage() {
        return formAddLanguage;
    }
}
