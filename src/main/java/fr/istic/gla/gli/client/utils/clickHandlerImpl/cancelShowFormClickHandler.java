package fr.istic.gla.gli.client.utils.clickHandlerImpl;

import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import fr.istic.gla.gli.client.tp3_gwtUi;

/**
 * Created by lautre on 18/01/16.
 */
public class cancelShowFormClickHandler implements ClickHandler{

    private final tp3_gwtUi parent;
    private String entity;

    public cancelShowFormClickHandler(tp3_gwtUi parent, String entity) {
        this.parent = parent;
        this.entity = entity;
    }

    public void onClick(ClickEvent clickEvent) {
        if (entity.equals("language")) parent.getFormAddLanguage().getElement().getStyle().setDisplay(Style.Display.NONE);
    }
}
