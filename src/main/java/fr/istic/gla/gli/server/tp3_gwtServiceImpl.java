package fr.istic.gla.gli.server;

import com.google.gwt.dev.json.JsonObject;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import fr.istic.gla.gli.client.service.tp3_gwtService;
import fr.istic.gla.gli.server.utils.UtilsHTTP;

import java.util.HashMap;

@RemoteServiceRelativePath("getData")
public class tp3_gwtServiceImpl extends RemoteServiceServlet implements tp3_gwtService {

    private final String host = "http://localhost:8080";

    public String getAll(String entity) throws Exception {
        return UtilsHTTP.sendGet(host+"/"+entity+"/all");
    }

    public String getOneById(String entity,String id) throws Exception {
        return UtilsHTTP.sendGet(host+"/"+entity+"/"+id);
    }

    public void create(String entity, HashMap<String,String> data) throws Exception {
        if (entity.equals("sprint")){
            UtilsHTTP.sendPost(host+"/"+entity+"/create/"+data.get("name"),null,null);
        } else if (entity.equals("language")){
            String url = host+"/"+entity+"/create/"+data.get("name");
            String idUser;
            if ((idUser = data.get("idUser"))!= null) url += "/"+idUser;
            UtilsHTTP.sendPost(url,null,null);
        } else if (entity.equals("task")){
            JsonObject body = new JsonObject();
            body.put("name",data.get("name"));
            body.put("description",data.get("description"));
            UtilsHTTP.sendPost(host+"/"+entity+"/create/"+data.get("userStorieId"),body.toString(),null);
        } else if (entity.equals("user")){
            JsonObject body = new JsonObject();
            body.put("firstname",data.get("firstname"));
            body.put("lastname",data.get("lastname"));
            UtilsHTTP.sendPost(host+"/"+entity+"/create/",body.toString(),null);
        } else if (entity.equals("userStorie")){
            JsonObject body = new JsonObject();
            body.put("name",data.get("name"));
            body.put("description",data.get("description"));
            UtilsHTTP.sendPost(host+"/"+entity+"/create/"+data.get("sprintId")+"/"+data.get("userId"),body.toString(),null);
        }
    }

    public void deleteOneById(String entity, String id) throws Exception {
        UtilsHTTP.sendDelete(host+"/"+entity+"/"+id);
    }
}
